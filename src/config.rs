pub static VERSION: &str = "0.1.0";
pub static GETTEXT_PACKAGE: &str = "tnd4core";
pub static LOCALEDIR: &str = "/app/share/locale";
pub static PKGDATADIR: &str = "/app/share/tnd4core";
