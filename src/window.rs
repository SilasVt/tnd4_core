/* window.rs
 *
 * Copyright 2022 SilasV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::subclass::prelude::*;
use gtk::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/SilasVt/gitlab/TND4Core/window.ui")]
    pub struct Tnd4coreWindow {
        // Template widgets
        /*    #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
        */
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Tnd4coreWindow {
        const NAME: &'static str = "Tnd4coreWindow";
        type Type = super::Tnd4coreWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Tnd4coreWindow {}
    impl WidgetImpl for Tnd4coreWindow {}
    impl WindowImpl for Tnd4coreWindow {}
    impl ApplicationWindowImpl for Tnd4coreWindow {}
    impl AdwApplicationWindowImpl for Tnd4coreWindow {}
}

glib::wrapper! {
    pub struct Tnd4coreWindow(ObjectSubclass<imp::Tnd4coreWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,        @implements gio::ActionGroup, gio::ActionMap;
}

impl Tnd4coreWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)]).expect("Failed to create Tnd4coreWindow")
    }
}

